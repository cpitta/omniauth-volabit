require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Volabit < OmniAuth::Strategies::OAuth2
      option :name, 'volabit'
      option :client_options, {
          :site => 'https://www.volabit.com',
          :proxy => ENV['http_proxy'] ? URI(ENV['http_proxy']) : nil, 
          :access_token_url   => 'http://www.volabit.com/oauth/token'
      }

      uid { raw_info['email'] }

      info do
        { 
            :name => raw_info['first_name'],
            :email => raw_info['email']
        }
      end

      def callback_url
        options[:redirect_uri] || (full_host + script_name + callback_path)
      end

      extra do
        { :raw_info => raw_info }
      end

      def raw_info
        @raw_info ||= MultiJson.load(access_token.get('/api/v1/users/me').body)
      rescue ::Errno::ETIMEDOUT
        raise ::Timeout::Error
      end

    end
  end
end